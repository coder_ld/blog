package net.sppan.blog.job;


import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class Minute2Job implements Job {


	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("JobName2: {}" + context.getJobDetail().getKey().getName());
	}

}
