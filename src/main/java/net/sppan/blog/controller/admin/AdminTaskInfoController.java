package net.sppan.blog.controller.admin;

import com.alibaba.fastjson.JSON;
import net.sppan.blog.common.JsonResult;
import net.sppan.blog.entity.TaskInfo;
import net.sppan.blog.service.impl.TaskServiceImpl;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 任务管理
 * @author lance
 */
@Controller
@RequestMapping("/admin/task")
public class AdminTaskInfoController {

    @Resource
    private TaskServiceImpl taskServiceImpl;

    /**
     * Index.jsp
     * 2016年10月8日下午6:39:15
     */
    @RequestMapping("/index")
    public String info(){
        return "admin/task/index";
    }

    /**
     * 任务列表
     * @return
     * 2016年10月9日上午11:36:03
     */
    @ResponseBody
    @RequestMapping("/list")
    public String list(){
        Map<String, Object> map = new HashMap<>();
        List<TaskInfo> infos = taskServiceImpl.list();
        map.put("rows", infos);
        map.put("total", infos.size());
        System.out.println(infos.size());
        System.out.println(infos);
        return JSON.toJSONString(map);
    }
    @RequestMapping("/form")
    public String from(@RequestParam(value = "id",required = false)Integer id , ModelMap map) {
        List<TaskInfo> infos = taskServiceImpl.list();
        if(id != null) {
            TaskInfo info = infos.get(id);
            map.addAttribute("info",info);
        }
        return "admin/task/form";
    }

    /**
     * 保存定时任务
     * @param info
     * 2016年10月9日下午1:36:59
     */
    @ResponseBody
    @RequestMapping(value="save", method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public JsonResult save(TaskInfo info){
        System.out.println(info);
        try {
            if(info.getId() == null) {
                System.out.println("添加");
                taskServiceImpl.addJob(info);
            }else{
                System.out.println("修改");
                taskServiceImpl.edit(info);
            }
        } catch (ServiceException e) {
            return JsonResult.fail(e.getMessage());
        }
        return JsonResult.ok();
    }

    /**
     * 删除定时任务
     * @param jobName
     * @param jobGroup
     * 2016年10月9日下午1:52:20
     */
    @ResponseBody
    @RequestMapping(value="delete/{jobName}/{jobGroup}", produces = "application/json; charset=UTF-8")
    public JsonResult delete(@PathVariable String jobName, @PathVariable String jobGroup){
        try {
            taskServiceImpl.delete(jobName, jobGroup);
        } catch (ServiceException e) {
            return JsonResult.fail(e.getMessage());
        }
        return JsonResult.ok();
    }

    /**
     * 暂停定时任务
     * @param jobName
     * @param jobGroup
     * 2016年10月10日上午9:41:25
     */
    @ResponseBody
    @RequestMapping(value="pause/{jobName}/{jobGroup}", produces = "application/json; charset=UTF-8")
    public JsonResult pause(@PathVariable String jobName, @PathVariable String jobGroup){
        try {
            taskServiceImpl.pause(jobName, jobGroup);
        } catch (ServiceException e) {
            return JsonResult.fail(e.getMessage());
        }
        return JsonResult.ok();
    }
    /**
     * 重新开始定时任务
     * @param jobName
     * @param jobGroup
     * 2016年10月10日上午9:41:40
     */
    @ResponseBody
    @RequestMapping(value="resume/{jobName}/{jobGroup}", produces = "application/json; charset=UTF-8")
    public JsonResult resume(@PathVariable String jobName, @PathVariable String jobGroup){
        try {
            taskServiceImpl.resume(jobName, jobGroup);
        } catch (ServiceException e) {
            return JsonResult.fail(e.getMessage());
        }
        return JsonResult.ok();
    }
}
