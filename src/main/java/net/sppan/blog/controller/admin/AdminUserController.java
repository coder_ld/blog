package net.sppan.blog.controller.admin;

import net.sppan.blog.common.JsonResult;
import net.sppan.blog.controller.BaseController;
import net.sppan.blog.entity.User;
import net.sppan.blog.service.UserService;
import net.sppan.blog.utils.IpKit;
import net.sppan.blog.utils.MD5Kit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/user")
public class AdminUserController extends BaseController {
	@Resource
	private UserService userService;

	@GetMapping("/index")
	public String index() {
		return "admin/user/index";
	}

	@PostMapping("/list")
	@ResponseBody
	public Page<User> list() {
		PageRequest pageRequest = getPageRequest();
		Page<User> page = userService.findAll(pageRequest);
		return page;
	}

	@GetMapping("/form")
	public String form(@RequestParam(required=false) Long id,
			ModelMap map
			){
		if(id != null){
			User user = userService.findById(id);
			map.put("user", user);
		}
		return "admin/user/form";
	}
	
	@PostMapping("/save")
	@ResponseBody
	public JsonResult save(User user, HttpServletRequest request){
		String password  = null;
		User loginUser = getLoginUser();
		if(user.getId() != null) {
			if(!loginUser.getPassword().equals(user.getPassword())) {
				password = MD5Kit.generatePasswordMD5(user.getPassword(),user.getSalt());
			} else {
				password = loginUser.getPassword();
			}
		} else {
		      password =MD5Kit.generatePasswordMD5(user.getPassword(),user.getSalt());
		}

		try {
			String ip = IpKit.getRealIp(request);
			user.setPassword(password);
			user.setIp(ip);
			userService.saveOrUpdate(user);
		} catch (Exception e) {
			e.printStackTrace();
			return JsonResult.fail(e.getMessage());
		}
		return JsonResult.ok();
	}
	
	@PostMapping("/{id}/del")
	@ResponseBody
	public JsonResult delete(
			@PathVariable Long id
			){
		try {
			userService.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			return JsonResult.fail(e.getMessage());
		}
		return JsonResult.ok();
	}
}
